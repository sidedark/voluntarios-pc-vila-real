<?php
session_start();
include("includes/config.php");
if(!isset($_SESSION["admin"]) && !isset($_SESSION["usuario"])){
  if(isset($_SESSION["usuario"]) && ($_SESSION["DNI"])){
      header("Location: panelvoluntarios.php");
  } 
   header("Location: login.php");
 }

 
?>

<!doctype html>
<html lang="en" class="h-100">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <title>Protección Civil - Alta de voluntario</title>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <!-- Bootstrap core CSS -->
<link href="css/bootstrap.css" rel="stylesheet">


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="css/sticky-footer-navbar.css" rel="stylesheet">
  </head>
  <body class="d-flex flex-column h-100">
    <header>
        <!-- Fixed navbar -->
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <a class="navbar-brand" href="#">Protección Civil</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
              <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                  <a class="nav-link" href="#">Inicio <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">Información</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="servicios.html">Servicios</a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link" href="#">Panel de administración</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Colaboraciones</a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link" href="#">Legislación</a>
                        </li>
              </ul>
            </div>
          </nav>
</header>

<!-- Begin page content -->
<main role="main" class="flex-shrink-0">
  <div class="container">
  <form class="form-horizontal" action="<?php echo $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data" method="POST">
        <fieldset>
          <legend>Añadir a un voluntario a un equipo</legend>
          <div class="form-row">
            <div class="form-group col-md-6">
                <label for="tiposervicio">Voluntarios</label>
                    <select multiple class="form-control" name="voldisp" id="voldisp" onchange="getEditVol(this.value);$( 'div').remove( '#newvol' );">
                <?php
                 $q = "SELECT *,CONCAT(Nombre,' ',Apellidos) as NombreCompleto FROM personalpc WHERE Activo=1";   
                 $res = mysqli_query($sql,$q);
                 $fila2 = mysqli_fetch_assoc($sql,$res);
                 while($fila=mysqli_fetch_assoc($res)){
                    echo "<option value=".$fila['DNI'].">".$fila['NombreCompleto']."</option>";
                 }
                ?>
               </select>
            </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                <select multiple class="form-control" name="equiposdisp" id="equiposdisp">
                <?php
               $q = "SELECT idEquipo,CONCAT(idEquipo,' - ',Descripcion) as Descripcion FROM equipos";
               $res = mysqli_query($sql,$q);
               while($fila=mysqli_fetch_assoc($res)){
                   echo "<option value='".$fila['idEquipo']."'>".$fila['Descripcion']."</option>";
               }
               ?>
                 </select>
            </div>
            <div class="form-group col-md-6">
          <button type="submit" class="btn btn-primary">Asignar a equipo</button>
          </div>
         </div>
  </div>
</main>
    
         <footer class="footer mt-auto py-3">
  <div class="container">
    <span class="text-muted">Place sticky footer content here.</span>
  </div>
</footer>
</body>
</html>
<?php
if($_SERVER['REQUEST_METHOD'] == 'POST'){
  $idEquipo = $_POST["equiposdisp"];
  $dni = $_POST["voldisp"];
  $q = "UPDATE personalpc SET Equipo = '".$idEquipo."' WHERE DNI = '".$dni."'";  
  $res = mysqli_query($sql,$q);
  if(!$res){
    echo "<p>Error en la consulta!</p>";
    echo $idEquipo;
    echo mysqli_error($sql);
  } else{
    echo "<script>alert('Asignado con exito')</script>";
    echo "<meta http-equiv='refresh' content='0'>";
    }
  }
?>