<?php
session_start();
include("includes/config.php");
if(empty($_SESSION["usuario"]) || empty($_SESSION["DNI"]) || empty($_SESSION["admin"])){
  if(!empty($_SESSION["usuario"]) && !empty($_SESSION["DNI"])){
      header("Location: panelvoluntarios.php");
  } 
   header("Location: login.php");
 }
 //Se obtiene el DNI del voluntario de la sesion
$dni = $_SESSION["DNI"];
//Se definen variables para las consultas
$q2 = "SELECT * FROM servicios WHERE Activo=1";
$q  = "SELECT DISTINCT servicios.idServicio,CONCAT(nombreServicio,' - ',DATE_FORMAT(fechaServicio,'%d-%m-%Y')) as Servicios FROM servicios INNER JOIN apuntado_servicio ON servicios.idServicio=apuntado_servicio.idServicio WHERE apuntado_servicio.DNI = '".$dni."' AND servicios.Activo=1";
//Se realizan las consultas
$res = mysqli_query($sql,$q2);
$res2 = mysqli_query($sql,$q);
//Se obtiene el numero de registros obtenidos para recorrer el array
// Y se obtiene el ID de servicio
$long = mysqli_num_rows($res2);
$fila2=mysqli_fetch_assoc($res2);
//Se crea y se recorre el array
$activos = array();
//echo "Longitud de array: ".$long."\n";
for ($i=0; $i < $long; $i++) { 
 $activos[$i] = $fila2['idServicio'];
 //echo "Recorrido registro ".$i." veces";
}
?>
<!doctype html>
<html lang="en" class="h-100">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <title>Protección Civil - Servicios</title>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <!-- Bootstrap core CSS -->
<link href="css/bootstrap.css" rel="stylesheet">


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="css/sticky-footer-navbar.css" rel="stylesheet">
  </head>
  <body class="d-flex flex-column h-100">
    <header>
  <!-- Fixed navbar -->
  <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="#">Protección Civil</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="index.php">Inicio <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Información</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="login.php">Servicios</a>
          </li>
          <li class="nav-item">
              <a class="nav-link" href="login.php">Panel de administración</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Colaboraciones</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="#">Legislación</a>
                </li>
      </ul>
    </div>
  </nav>
</header>

<!-- Begin page content -->
<main role="main" class="flex-shrink-0">
  <div class="container">
  <form name="altaservicio" class="form-horizontal" action="<?php echo $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data" method="POST">
    <fieldset>
    <legend>Servicios disponibles</legend>
    </fieldset>
        <div class="form-row">
            <div class="form-group col-lg-8">
                <label for="tiposervicio">Servicios disponibles</label>
                    <select multiple class="form-control" name="serviciosdisp" id="serviciosdisp" onchange="getDatosServicio(this.value);getDatosVoluntario(this.value);">
                <?php
                while($fila=mysqli_fetch_assoc($res)){
                 echo "<option value='".$fila['idServicio']."'>".$fila['nombreServicio']."</option>";
               }
                ?>
               </select>
            </div>
        </div>
        <div class="form-row">
                 <div id="datosser" name="datosser" class="form-group col-lg-8">
                
                </div>
                <script>
                    function getDatosServicio(valor){
                       $.ajax({
                        url: "includes/dserv1.php",
                        type: 'GET',
                        data: {tipo_id:valor},
                           success: function(result) {
                               //console.log(result);
                             $('#datosser').html(result);
                               },
                       error: function(request, error, message) {
                        // error
                       }
                        });
                    }
                    </script>
        </div>
        <form name="altaservicio" class="form-horizontal" action="<?php echo $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data" method="POST">
    <fieldset>
    <legend>Voluntarios apuntados</legend>
    </fieldset>
        <div class="form-row">
            <div class="form-group col-md-6">
                    <select multiple class="form-control" name="voldisp" id="voldisp">
                
               </select>
               <button type="submit" class="btn btn-primary">Borrar del servicio</button>
               <script>
                    function getDatosVoluntario(valor){
                       $.ajax({
                        url: "includes/dvapu.php",
                        type: 'GET',
                        data: {idServicio:valor},
                           success: function(result) {
                               console.log(result);
                             $('#voldisp').html(result);
                               },
                       error: function(request, error, message) {
                        // error
                       }
                        });
                    }
                    </script>
            </div>
        </div>
        <div>
        <?php
         if($_SERVER['REQUEST_METHOD'] == 'POST'){
          $dnivol = $_POST["voldisp"];
          $q3 = "DELETE FROM apuntado_servicio WHERE DNI = '".$dnivol."'";
          $res = mysqli_query($sql,$q3);
         }
         if(!$res){
          $msg = "";
          $msg.= "<div class='form-group col-md-6'>";
          $msg.="<div class='alert alert-danger' role='alert'>
        Error al borrar el registro: ";
        $msg .= mysqli_error($sql);
        $msg.="</div>";
        echo $msg;
        } else{
          if($_SERVER['REQUEST_METHOD'] == 'POST'){
            echo "";
          } else{
          $msg = "";
          $msg .= "<div class='form-group col-md-6'>";
          $msg .= "<div class='alert alert-error' role='alert'>
          No has seleccionado un voluntario!
        </div>";
        $msg .= "</div>";
        echo $msg;
        }
        }
        ?>
        </div>
        <div class="form-group col-md-6">
                <a href="paneladmin.php" class="btn btn-primary">Volver al panel</a>
            </div>
  </div>
</main>

<footer class="footer mt-auto py-3">
  <div class="container">
    <span class="text-muted">Place sticky footer content here.</span>
  </div>
</footer>
 <script src="js/bootstrap.bundle.min.js"></script></body>
</html>
