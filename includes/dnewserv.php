<?php
include("config.php");
if (isset($_POST['new'])) {
    $new = 1;
} else {
    $new = 0;
}
?>    
<div class="form-row">
                    <div class="form-group col-md-6">
                    <input type="hidden" name="newreg" id="newreg" value=1>
                        <label for="tiposervicio">Tipo del servicio</label>
                        <select class="form-control" name="tiposervicio" id="tiposervicio" onchange="getSubtipos(this.value);">
                            <?php
                            $q2 = "SELECT * FROM tipos_servicio";
                            $resu = mysqli_query($sql,$q2);
                            while($fila = mysqli_fetch_assoc($resu)){
                              echo "<option value='".$fila['idTipo']."'>".$fila['Descripcion']."</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="subtiposervicio">Subtipo del servicio</label>
                        <select class="form-control" name="subtiposervicio" id="subtiposervicio">

                        </select>
                    </div>
                    <script>
                    function getSubtipos(valor){
                       $.ajax({
                        url: "includes/dsubtipo.php",
                        type: 'GET',
                        data: {tipo_id:valor},
                           success: function(result) {
                               //console.log(result);
                             $('#subtiposervicio').html(result);
                               },
                       error: function(request, error, message) {
                        // error
                       }
                        });
                    }
                    </script>
                    <div class="form-group col-md-6">
                        <label for="nombreservicio">Nombre del Servicio</label>
                        <input type="text" class="form-control" id="nombreservicio" name="nombreservicio">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="fechaservicio">Fecha del servicio</label>
                        <input type="date" class="form-control" id="fechaservicio" name="fechaservicio" value="<?php date('d-m-Y');?>">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="nombre">Hora de inicio en Base</label>
                        <input type="text" class="form-control" id="horabase" name="horabase">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="apellidos">Hora de inicio del Servicio</label>
                        <input type="text" class="form-control" id="horainicio" name="horainicio">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="direccion">Hora de finalización del Servicio</label>
                        <input type="text" class="form-control" id="horafin" name="horafin">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="telefono">Observaciones</label>
                        <textarea rows="5" cols="50" name="observaciones"></textarea>
                    </div>
		<div class="form-group col-md-6">
			<label for="activo">Activo</label>
			<input type="checkbox" value="1" name="activo">
		</div>
                <div class="form-group col-md-6">
                    <button type="submit" class="btn btn-primary" name="submit" value=1>Dar de alta</button>
                </div>
                <div class="form-group col-md-6">
                <a href="paneladmin.php" class="btn btn-primary">Volver al panel</a>
            </div>
        </form>
        </fieldset>
    </div>
<?php
   global $sql;
   if($_POST["submit"] == 1){
       //Se recogen los datos del formulario
       $tiposer = $_POST["tiposervicio"];
       $subtiposer = $_POST["subtiposervicio"];
       $nameser = $_POST["nombreservicio"];
       $dateser = $_POST["fechaservicio"];
       $hsede = $_POST["horabase"];
       $hinicio = $_POST["horainicio"];
       $hfin = $_POST["horafin"];
       $activo = (int)($_POST["activo"]);
       $obs = mysqli_real_escape_string($sql,$_POST["observaciones"]);
       // Debug
       
       // Se realiza la consulta a la base de datos
       $q1 = "INSERT INTO servicios (nombreServicio,fechaServicio,horaSede,horaInicio,horaFin,TipoServicio,SubtipoServicio,Observaciones,Activo) VALUES ('$nameser','$dateser','$hsede','$hinicio','$hfin',$tiposer,$subtiposer,'$obs',$activo)";
       $res = mysqli_query($sql,$q1);
       if(!$res){
           echo "<p>Error en la consulta!</p>";
           echo $nameser;
           echo mysqli_error($sql);

       } else{
           echo "<script>alert('Insertado con exito')</script>";
           echo "<meta http-equiv='refresh' content='0'>";
       }
   }
?>

