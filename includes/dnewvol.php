<?php
include("config.php");
if (isset($_POST['new'])) {
    $new = 1;
} else {
    $new = 0;
}
?>  
<div class="container">
  <input type="hidden" name="newreg" value=1>
    <form class="form-horizontal" action="<?php echo $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data" method="POST">
    <fieldset>
          <legend>Alta de nuevo voluntario</legend>
          <div class="form-row">
          <div class="form-group col-md-6">
                  <label for="dni">DNI</label>
                  <input type="text" class="form-control" id="dni" name="dni" required>
          </div>
            <div class="form-group col-md-6">
                <label for="nombre">Nombre</label>
                <input type="text" class="form-control" id="nombre" name="nombre">
              </div>
              <div class="form-group col-md-6">
                <label for="apellidos">Apellidos</label>
                <input type="text" class="form-control" id="apellidos" name="apellidos">
              </div>
              <div class="form-group col-md-6">
                <label for="direccion">Dirección</label>
                <input type="text" class="form-control" id="direccion" placeholder="Dirección" name="direccion">
              </div>
              <div class="form-group col-md-6">
                <label for="telefono">Telefono</label>
                <input type="text" class="form-control" id="telefono" placeholder="Telefono" name="telefono">
              </div>
              <div class="form-group col-md-6">
                <label for="gruposang">Grupo Sanguineo</label>
                <input type="text" class="form-control" id="gruposang" name="gruposang">
              </div>
              <div class="form-group col-md-6">
                <label for="email">Email</label>
                <input type="email" class="form-control" id="email" name="email">
              </div>
              <div class="form-group col-md-6">
              <label for="indicativo">Indicativo</label>
              <input type="text" class="form-control" id="indicativo" name="indicativo" requiered>
            </div>
              <div class="form-group col-md-6">
                <label for="foto">Foto</label>
                <input type="file" class="form-control" id="foto" name="foto">
              </div>
              <div class="form-group col-md-6">
                <label for="estudios">Estudios</label>
                <input type="text" class="form-control" id="estudios" name="estudios">
              </div>
              <div class="form-group col-md-6">
                <label for="profesion">Profesion</label>
                <input type="text" class="form-control" id="profesion" name="profesion">
              </div>
              <div class="form-group col-md-6">
                  <label for="fechanaci">Fecha de nacimiento</label>
                  <input type="date" class="form-control" id="fechanaci" name="fechanaci" value="<?php date(d-m-Y);?>">
              </div>
              <div class="form-group col-md-6">
                  <label for="cpostal">Codigo Postal</label>
                  <input type="text" class="form-control" id="cpostal" name="cpostal">
              </div>
              <div class="form-group col-md-6">
                  <label for="poblacion">Poblacion</label>
                  <input type="text" class="form-control" id="poblacion" name="poblacion">
              </div>
              <div class="form-group col-md-6">
                  <label for="provincia">Provincia</label>
                  <input type="text" class="form-control" id="provincia" name="provincia">
              </div>
              <div class="form-group col-md-6">
                  <label for="caducidadcarne">Fecha de caducidad del Carnet</label>
                  <input type="text" class="form-control" id="caducidadcarne" name="caducidadcarne">
              </div>
              <div class="form-group col-md-6">
              <label for="nombreservicio">Usuario</label>
              <input type="text" class="form-control" id="username" name="username" requiered>
            </div>
            <div class="form-group col-md-6">
              <label for="passwd">Contraseña</label>
              <input type="password" class="form-control" id="passwd" name="passwd" required>
            </div>
              <div class="form-group col-md-6">
                  <label for="fechadealta">Fecha de alta</label>
                  <input type="date" class="form-control" id="fechadealta" name="fechadealta" value="<?php date(d-m-Y);?>">
              </div>
              <div class="form-group col-md-6">
              <label for="fechadealta">Tipo de voluntario</label>
                <select class="form-control" name="tipovoluntario" id="tipovoluntario">
                <?php
                     $q2 = "SELECT * FROM tipos_voluntario";
                     $resu = mysqli_query($sql,$q2);
                     while($fila = mysqli_fetch_assoc($resu)){
                     echo "<option value='".$fila['idTipoVoluntario']."'>".$fila['nombreTipoVoluntario']."</option>";
                            }
                ?>
                </select>
              </div>
            </div>
          <div class="form-group col-md-6">
            <div class="form-check">
              <input type="hidden" id="activo" value="0">
	      <input class="form-check-input" type="checkbox" id="activo" value="1">
              <label class="form-check-label" for="activo">
                Activo
              </label>
            </div>
            <div class="form-check">
		<input type="hidden" id="profesional" value="0">
                <input class="form-check-input" type="checkbox" id="profesional" value="1">
                <label class="form-check-label" for="profesional">
                  Profesional
                </label>
              </div>
              <div class="form-check">
		<input type="hidden" id="telegram" value="0">
                <input class="form-check-input" type="checkbox" id="telegram" value="1">
                <label class="form-check-label" for="telegram">
                  Telegram
                </label>
              </div>
          </div>
          <div class="form-group col-md-6">
          <button type="submit" class="btn btn-primary">Dar de alta</button>
          </div>
        </form>
    </fieldset>
  </div>
</main>
<?php
function darDeAlta(){
global $sql;
if($_SERVER['REQUEST_METHOD'] == 'POST'){
    //Se recogen los datos del formulario
    $usr = $_POST["username"];
    $passwd = $_POST["passwd"];
    $name = $_POST["nombre"];
    $surname = $_POST["apellidos"];
    $addr = $_POST["direccion"];
    $phone = $_POST["telefono"];
    $grusang = $_POST["gruposang"];
    $email = $_POST["email"];
    $ind = $_POST["indicativo"];
    $pic = $_POST["foto"];
    $estudios = $_POST["estudios"];
    $profesion = $_POST["profesion"];
    $activo = (int)($_POST["activo"]);
    $profesional = (int)($_POST["profesional"]);
    $telegram = (int)($_POST["telegram"]);
    $voluntario = (int)($_POST["voluntario"]);
    $dni = $_POST["dni"];
    $fechanac = $_POST["fechanaci"];
    $cpost = $_POST["cpostal"];
    $pob = $_POST["poblacion"];
    $pro = $_POST["provincia"];
    $fechalta = $_POST["fechadealta"];
    $tipovol = $_POST["tipovoluntario"];
    // Debug 
    echo $ind."\n";
    echo $dni;
    // Se realiza la consulta a la base de datos
$q1 = "INSERT INTO personalpc (Apellidos,Nombre,Direccion,Telefono,CorreoElectronico,GrupoSanguineo,Identificativo,Activo,Profesional,Estudios,Profesion,Telegram,usuario,contrasenya,DNI,Poblacion,Provincia,FechaNacimiento,CodigoPostal,FechaAlta,tipoVoluntario) 
VALUES ('$surname','$name','$addr','$phone','$email','$grusang','$ind',$activo,$profesional,'$estudios','$profesion','$telegram','$usr','$passwd','$dni','$pob','$pro','$fechanac','$cpost','$fechalta',$tipovol)";
    $res = mysqli_query($sql,$q1);
    if(!$res){
      $msg = "";
      $msg.= "<div class='form-group col-md-6'>";
      $msg.="<div class='alert alert-danger' role='alert'>
   Error al introducir los datos: ";
   if(mysqli_errno($sql)==1062){
       $msg.="Ya estas inscrito en este servicio!";
   }
   $msg.= mysqli_error($sql);
  $msg.="</div>";
  echo $msg;
  }else{
        echo "<script>alert('Insertado con exito')</script>";
    }
}

}
?>