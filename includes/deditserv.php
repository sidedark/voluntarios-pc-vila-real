<?php
include("config.php");
if (isset($_POST['edit'])) {
    $id = $_POST['edit'];
    $record = mysqli_query($sql, "SELECT * FROM servicios WHERE idServicio=$id");
    $new = 0;
    if (count($record) == 1 ) {
        $n = mysqli_fetch_array($record);
        $name = $n['nombreServicio'];
        $date = $n['fechaServicio'];
        $horaS = $n['horaSede'];
        $horaI = $n['horaInicio'];
        $horaF = $n['horaFin'];
        $tservicio = $n['TipoServicio'];
        $sbservicio = $n['SubtipoServicio'];
        $obs = $n['Observaciones'];
        $ac = $n['Activo'];
    }  
} 
?>
            <div class="form-group col-md-6">
                <label for="tiposervicio">Tipo del servicio</label>
                <?php
                if(isset($_POST['edit'])){
                    echo '<input type="hidden" name="idreg" id="idreg" value='.$_POST['edit'].'>';
                    echo '<input type="hidden" name="editreg" id="editreg" value=1>'; 
                } else {

                }
                ?>
                        <select class="form-control" name="tiposervicio" id="tiposervicio" onchange="getSubtipos(this.value);">
                            <?php
                            $q2 = "SELECT * FROM tipos_servicio";
                            $resu = mysqli_query($sql,$q2);
                            while($fila = mysqli_fetch_assoc($resu)){
                                if($n['TipoServicio']==$fila['idTipo']){
                                    $selected = 'selected="selected"';
                                } else {
                                    $selected = '';
                                }
                               echo "<option value=".$fila['idTipo'].$selected.">".$fila['Descripcion']."</option>";
                            }
                            ?>
                  </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="subtiposervicio">Subtipo del servicio</label>
                        <select class="form-control" name="subtiposervicio" id="subtiposervicio">

                        </select>
                    </div>
                    <script>
                    function getSubtipos(valor){
                       $.ajax({
                        url: "includes/dsubtipom.php",
                        type: 'GET',
                        data: {tipo_id:valor},
                           success: function(result) {
                               //console.log(result);
                             $('#subtiposervicio').html(result);
                               },
                       error: function(request, error, message) {
                        // error
                       }
                        });
                    }
                    </script>
            <div class="form-group col-md-6">
                        <label for="nombreservicio">Nombre del Servicio</label>
                        <input type="text" class="form-control" id="nombreservicio" name="nombreservicio" value="<?php echo $name;?>">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="fechaservicio">Fecha del servicio</label>
                        <input type="date" class="form-control" id="fechaservicio" name="fechaservicio" value="<?php echo $date;?>">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="nombre">Hora de inicio en Base</label>
                        <input type="text" class="form-control" id="horabase" name="horabase" value="<?php echo $horaS;?>">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="apellidos">Hora de inicio del Servicio</label>
                        <input type="text" class="form-control" id="horainicio" name="horainicio" value="<?php echo $horaI;?>">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="direccion">Hora de finalización del Servicio</label>
                        <input type="text" class="form-control" id="horafin" name="horafin" value="<?php echo $horaF;?>">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="telefono">Observaciones</label>
                        <textarea rows="5" cols="50" name="observaciones"><?php echo $obs;?></textarea>
                    </div>
		<div class="form-group col-md-6">
			<?php
            if($n["Activo"]!=1){
                $check = "<input class='form-check-input' type='checkbox' name='activo' id='activo' value=1 >";   
            } else {
                $check = "<input class='form-check-input' type='checkbox' name='activo' id='activo' value=1 checked>";
            }
             echo $check;
            ?>
            <label for="activo">Activo</label>
		</div>
                <div class="form-group col-md-6">                
                    <button type="submit" class="btn btn-primary" name="submit" value=1>Guardar cambios</button>
                </div>
                <div class="form-group col-md-6">
                <a href="paneladmin.php" class="btn btn-primary">Volver al panel</a>
            </div>
                </div>
        <?php        
        global $sql;
    if($_POST["submit"] == 1){
        //Se recogen los datos del formulario
        $id = $_POST['idreg'];
        $tiposer = $_POST["tiposervicio"];
        $subtiposer = (int)($_POST["subtiposervicio"]);
        $nameser = $_POST["nombreservicio"];
        $dateser = $_POST["fechaservicio"];
        $hsede = $_POST["horabase"];
        $hinicio = $_POST["horainicio"];
        $hfin = $_POST["horafin"];
	$activo = (int)($_POST["activo"]);
        $obs = mysqli_real_escape_string($sql,$_POST["observaciones"]);
        // Debug
        
        // Se realiza la consulta a la base de datos
        $q1 = "UPDATE servicios SET nombreServicio='$nameser',fechaServicio='$dateser',horaSede='$hsede',horaInicio='$hinicio',horaFin='$hfin',TipoServicio='$tiposer',SubtipoServicio=$subtiposer,Observaciones='$obs',Activo=$activo WHERE idServicio=$id";
        $res = mysqli_query($sql,$q1);
        if(!$res){
            echo "<p>Error en la consulta!</p>";
            echo mysqli_error($sql);
        } else{
            echo "<script>alert('Actualizado con exito')</script>";
            echo "<meta http-equiv='refresh' content='0'>";
        }
    }

?>
