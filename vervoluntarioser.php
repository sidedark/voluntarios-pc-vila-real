<?php
session_start();
include("includes/config.php");
if(!isset($_SESSION["usuario"]) && !isset($_SESSION["DNI"])){
    header("Location: login.php");
}

if(isset($_SESSION["DNI"])){
   $dni = $_SESSION["DNI"];
}

?>
<!doctype html>
<html lang="en" class="h-100">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <title>Protección Civil - Servicios</title>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="css/sticky-footer-navbar.css" rel="stylesheet">
</head>
<body class="d-flex flex-column h-100">
<header>
    <!-- Fixed navbar -->
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="#">Protección Civil</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="index.php">Inicio <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Información</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="login.php">Servicios</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="login.php">Panel de administración</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Colaboraciones</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Legislación</a>
                </li>
            </ul>
        </div>
    </nav>
</header>
<main role="main" class="flex-shrink-0">
    <div class="container">
        <?php
            //Debug
            //echo $dni;
        ?>
        <form name="vervoluntario" class="form-horizontal" action="<?php echo $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data" method="POST">
            <fieldset>
                <legend>Ver servicios a los que estoy apuntado</legend>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="tiposervicio">Servicios en los que estoy apuntado</label>
                        <select multiple class="form-control" name="serviciosdisp" id="serviciosdisp">
                            <?php
                                $qc = "SELECT servicios.idServicio,CONCAT(nombreServicio,' - ',DATE_FORMAT(fechaServicio,'%d-%m-%Y')) as Servicios FROM servicios INNER JOIN apuntado_servicio ON servicios.idServicio=apuntado_servicio.idServicio WHERE apuntado_servicio.DNI = '$dni' AND servicios.Activo=1";
                                $res = mysqli_query($sql,$qc);
                                while($fila = mysqli_fetch_assoc($res)){
                                    echo "<option value='".$fila['idServicio']."'>".$fila['Servicios']."</option>";
                                  }
                                ?>
                        </select>
                    </div>
                    <div class="form-row">
                 <div id="datosser" name="datosser" class="form-group col-md-6">
                
                </div>
                <script>
                    function getDatosServicio(valor){
                       $.ajax({
                        url: "includes/dshowserv.php",
                        type: 'GET',
                        data: {tipo_id:valor},
                           success: function(result) {
                               //console.log(result);
                             $('#datosser').html(result);
                               },
                       error: function(request, error, message) {
                        // error
                       }
                        });
                    }
                    </script>
                    <div class="form-group col-md-7">
                    <button type="submit" class="btn btn-primary">Desapuntarse</button>
                    </div>
                    <?php
                        if(empty($_SESSION["admin"])){
                   echo "<div class='form-group col-md-6'>
                       <a href='panelvoluntarios.php' class='btn btn-primary'>Volver al área de voluntario</a>
                    </div>";
                  } else {
               echo "<div class='form-group col-md-6'>
                    <a href='paneladmin.php' class='btn btn-primary'>Volver al área de administración</a>
                 </div>";
                  }
                 ?>
            </div>
            </fieldset>
  </div>
  </form>
  <?php
  if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $idserdap = $_POST["serviciosdisp"];
        $squery = "DELETE FROM apuntado_servicio WHERE DNI = '$dni' AND idServicio = $idserdap";
        $response = mysqli_query($sql,$squery);
        if(!$response){
            $msg = "";
            $msg.= "<div class='form-group col-md-6'>";
            $msg.="<div class='alert alert-danger' role='alert'>
         Error al realizar la consulta: ";
         $msg.= mysqli_error($sql);
        $msg.="</div>";
        echo $msg;
        } else{
            echo "<div class='form-group col-md-6'>";
            echo "<div class='alert alert-success' role='alert'>
            Te has desapuntado del servicio correctamente! :(
          </div>";
          echo "</div>";
        }
        echo "<meta http-equiv='refresh' content='0'>";
    }
?>
</main>

<footer class="footer mt-auto py-3">
  <div class="container">
    <span class="text-muted">Place sticky footer content here.</span>
  </div>
</footer>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
      <script>window.jQuery || document.write('<script src="/docs/4.3/assets/js/vendor/jquery-slim.min.js"><\/script>')</script><script src="/docs/4.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script></body>
</html>
