<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <title>Protección Civil - Login</title>
    <!-- Bootstrap core CSS -->
<link href="css/bootstrap.css" rel="stylesheet">


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="css/signin.css" rel="stylesheet">
  </head>
<body class="text-center">
    <form class="form-signin" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
    <img src="img/pcivil_logo.png" width="90" height="90"></img>
    <br>
    <br>
    <h1 class="h3 mb-3 font-weight-normal">Por favor, identificate</h1>
  <label for="username" class="sr-only">Usuario</label>
  <input type="text" id="username" class="form-control" name="username" placeholder="Usuario" required autofocus>
  <label for="passwd" class="sr-only">Password</label>
  <input type="password" id="passwd" class="form-control" placeholder="Contraseña" name="passwd" required>
  <button class="btn btn-lg btn-primary btn-block" type="submit">Entrar</button>
  <p class="mt-5 mb-3 text-muted">&copy; 2019-2020 - Cristian Garcia Quevedo</p>
</form>
</body>
</html>
<?php
include("includes/config.php");
function login(){
    session_start();
    global $sql;
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
       $usuario = $_POST["username"];
       $contrasenya = $_POST["passwd"];
       $q1 = "SELECT * FROM personalpc WHERE usuario = '$usuario' AND contrasenya = '$contrasenya'";
       $q2 = "SELECT DNI FROM personalpc WHERE usuario = '$usuario' AND contrasenya = '$contrasenya'";
       $dbq = mysqli_query($sql,$q1);
       $res = mysqli_fetch_assoc($dbq);
       $dbq2 = mysqli_query($sql,$q2);
       $res2 = mysqli_fetch_assoc($dbq2);
       $count = mysqli_num_rows($dbq);
       $admin = $res["administrador"];
       $dni = $res2["DNI"];
       //$_SESSION["usuario"] = $usuario;
       //$_SESSION["admin"] = $admin;
       //$_SESSION["DNI"] = $dni;
       /*setcookie("userlog",$usuario);
       setcookie("admin",$admin);
       setcookie("DNI",$dni);*/
       if($count==1 && $admin==1){
        $_SESSION["usuario"] = $usuario;
        $_SESSION["admin"] = $admin;
        $_SESSION["DNI"] = $dni;
         header("Location: paneladmin.php");
         exit();
       } else {
        $_SESSION["usuario"] = $usuario;
        $_SESSION["DNI"] = $dni;
        header("Location: panelvoluntarios.php");
        exit();
       }

}

}


login();
?>