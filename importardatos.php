<!doctype html>
<html lang="en" class="h-100">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <title>Protección Civil - Servicios</title>

    <!-- Bootstrap core CSS -->
<link href="css/bootstrap.css" rel="stylesheet">


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="css/sticky-footer-navbar.css" rel="stylesheet">
  </head>
  <body class="d-flex flex-column h-100">
    <header>
  <!-- Fixed navbar -->
  <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="#">Protección Civil</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="index.php">Inicio <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Información</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="login.php">Servicios</a>
          </li>
          <li class="nav-item">
              <a class="nav-link" href="login.php">Panel de administración</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Colaboraciones</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="#">Legislación</a>
                </li>
      </ul>
    </div>
  </nav>
</header>

<!-- Begin page content -->
<main role="main" class="flex-shrink-0">
  <div class="container">
  <form class="form-horizontal" action="<?php echo $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data" method="POST">
    <fieldset>
          <legend>Importar datos desde Access</legend>
          <div class="form-row">
            <div class="form-group col-md-6">
            <label for="tiposervicio">Tipo del servicio</label>
            <select class="form-control" name="tablaselec" id="tablaselec" onchange="getSubtipos(this.value);">
              <option value="apuntado_servicio">apuntado_servicio</option>
              <option value="cargos">cargos</option>
              <option value="equipos">equipos</option>
              <option value="personalpc">personalpc</option>
              <option value="servicios">servicios</option>
              <option value="tipos_servicio">tipos_servicio</option>
              <option value="serviciostipo">serviciostipo</option>
              <option value="tipos_voluntario">tipos_voluntario</option>
            </select>
  </div>
  <div class="form-group col-md-6">
  <input type="file" id="csv" name="csv" accept=".csv">
  </div>
</main>

<footer class="footer mt-auto py-3">
  <div class="container">
    <span class="text-muted">Place sticky footer content here.</span>
  </div>
</footer>
</body>
</html>
<?php
if (isset($_POST['enviar']))
{

  $tabla = $_POST['tablaselec'];


  $filename=$_FILES["file"]["name"];
  $info = new SplFileInfo($filename);
  $extension = pathinfo($info->getFilename(), PATHINFO_EXTENSION);

   if($extension == 'csv')
   {
	$filename = $_FILES['csv']['tmp_name'];
	$handle = fopen($filename, "r");

	while( ($data = fgetcsv($handle, 1000, ";") ) !== FALSE )
	{
    
    if($_POST['tablaselec']=="apuntado_servicio"){

      $q = "INSERT INTO ".$tabla."(DNI, idServicio) VALUES (
        '$data[0]', 
         $data[1]
      )";
    } elseif ($_POST['tablaselec']=="cargos") {
      $q = "INSERT INTO ".$tabla."(idCargo, Descripcion) VALUES (
         $data[0], 
        '$data[1]'
      )";
    } elseif($_POST['tablaselec']=="equipos"){
      $q = "INSERT INTO ".$tabla."(idEquipo, Descripcion) VALUES (
       '$data[0]', 
       '$data[1]'
     )";
    } elseif($_POST['tablaselec']=="personalpc"){
      $q = "INSERT INTO ".$tabla."(Apellidos,Nombre,DNI,Equipo,Observaciones,FechaNacimiento,Direccion,CodigoPostal,Poblacion,Provincia,Telefono,Movil,CorreoElectronico,Estudios,Profesion,Identificativo,Activo,Profesional,FechaAlta,FechaRenovacion,Cargo,FechaBaja,Telegram,CaducidadCarnet,usuario,contrasenya,administrador,GrupoSanguineo,tipoVoluntario) VALUES (
        '$data[0]', 
        '$data[1]',
        '$data[2]',
        '$data[3]',
        '$data[4]',
        '$data[5]',
        '$data[6]',
        '$data[7]',
        '$data[8]',
        '$data[9]',
        '$data[10]',
        '$data[11]',
        '$data[12]',
        '$data[13]',
        '$data[14]',
        '$data[15]',
        '$data[16]',
        '$data[17]',
        '$data[18]',
        '$data[19]',
        '$data[20]',
        '$data[21]',
        '$data[22]',
        '$data[23]',
        '$data[24]',
        '$data[25]',
        '$data[26]',
        '$data[27]',
        '$data[28]'
      )";
    } elseif($_POST['tablaselec']=="servicios"){
      $q = "INSERT INTO ".$tabla."(idServicio,nombreServicio,fechaServicio,horaSede,horaInicio,horaFin,TipoServicio,SubtipoServicio,Observaciones,Activo) VALUES (
        $data[0], 
        '$data[1]',
        '$data[2]',
        '$data[3]',
        '$data[4]',
        '$data[5]',
        '$data[6]',
        '$data[7]',
        '$data[8]',
        $data[9]
        )";
    } elseif($_POST['tablaselec']=="serviciostipo"){
      $q = "INSERT INTO ".$tabla."(idSubtipo,Descripcion,codSer) VALUES (
        $data[0], 
        '$data[1]',
        '$data[2]',
        '$data[3]'
        )";
    } elseif($_POST['tablaselec']=="tipos_servicio"){
      $q = "INSERT INTO ".$tabla."(idTipo,Descripcion) VALUES (
        $data[0], 
        '$data[1]'
        )";
    } elseif($_POST['tablaselec']=="tipos_voluntario"){
      $q = "INSERT INTO ".$tabla."(idTipoVoluntario,nombreTipoVoluntario) VALUES (
        $data[0], 
        '$data[1]'
        )";
    } else{
      echo "Error! No has seleccionado ninguna tabla o ha habido un error en la consulta!";
    }

	$mysqli->query($q);
   }

      fclose($handle);
   }
}
?>