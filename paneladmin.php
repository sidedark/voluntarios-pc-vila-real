<?php
session_start();
if(empty($_SESSION["usuario"]) || empty($_SESSION["DNI"]) || empty($_SESSION["admin"])){
   if(!empty($_SESSION["usuario"]) && !empty($_SESSION["DNI"])){
       header("Location: panelvoluntarios.php");
   } 
    header("Location: login.php");
  }
?>
<!doctype html>
<html lang="en" class="h-100">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <title>Protección Civil - Panel de administración</title>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/admin.css" rel="stylesheet">
    <link href="css/theme.css" rel="stylesheet">
    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="css/sticky-footer-navbar.css" rel="stylesheet">
</head>
<body class="d-flex flex-column h-100">
<header>
    <!-- Fixed navbar -->
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="#">Protección Civil</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="index.php">Inicio <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Información</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="servicios.html">Servicios</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Panel de administración</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Colaboraciones</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Legislación</a>
                </li>
            </ul>
        </div>
    </nav>
</header>
<!-- Begin page content -->
<main role="main" class="flex-shrink-0">
    <div class="container">
        <div class="row">
        <fieldset>
        <legend>Voluntarios</legend>
        <div class="col-md-7">
                <a class="btn btn-lg btn-primary" href="editarvoluntario.php" role="button">Editar o dar de alta/baja a un voluntario</a>
                <a class="btn btn-lg btn-primary" href="vervoluntario.php" role="button">Ver voluntarios en activo</a>
                <a class="btn btn-lg btn-primary" href="voluntarioapuntadoservicio.php" role="button">Voluntarios apuntados a servicios</a>
                <a class="btn btn-lg btn-primary" href="verhistoricovoluntarios.php" role="button">Ver historico de voluntarios</a>            </div>
        </fieldset>
        </div>
        <br>
        <div class="row">
        <fieldset>
        <legend>Servicios</legend>
        <div class="col-md-7">
            <a class="btn btn-lg btn-primary" href="verserviciosdb.php" role="button">Ver servicios almacenados</a>
            <a class="btn btn-lg btn-primary" href="editarservicio.php" role="button">Editar o dar de alta un servicio</a>
            </div>
        </fieldset>
        </div>
        <br>
        <div class="row">
        <fieldset>
        <legend>Equipos</legend>
        <div class="col-md-7">
            <a class="btn btn-lg btn-primary" href="verequiposdb.php" role="button">Ver equipos almacenados</a>
            <a class="btn btn-lg btn-primary" href="editarequipo.php" role="button">Editar o dar de alta un equipo</a>
            <a class="btn btn-lg btn-primary" href="asignarequipo.php" role="button">Asignar voluntario a equipo</a>
            </div>
        </fieldset>
        </div>
        <br>
        <div class="row">
        <fieldset>
        <legend>Mi cuenta</legend>
        <div class="col-md-7">
        <a class="btn btn-lg btn-primary" href="apuntarse.php" role="button">Apuntarse a un servicio</a>
        <a class="btn btn-lg btn-primary" href="vervoluntarioser.php" role="button">Ver servicios en los que estoy apuntado</a>
        <a class="btn btn-lg btn-primary" href="cerrar.php" role="button">Cerrar sesión</a>
        <a class="btn btn-lg btn-primary" href="importardatos.php" role="button">Importar datos</a>
        </div>
        </fieldset>
        </div>
    </div>
</main>
<footer class="footer mt-auto py-4">
    <div class="container">
        <span class="text-muted">Place sticky footer content here.</span>
    </div>
</footer>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery-slim.min.js"><\/script>')</script>
<script src="js/bootstrap.bundle.min.js"></script>
</body>
</html>