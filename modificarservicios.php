<?php
session_start();
include("includes/config.php");
if(!isset($_SESSION["usuario"]) && !isset($_SESSION["DNI"])){
  header("Location: login.php");
}
//Se definen variables para las consultas
$q = "SELECT idServicio,CONCAT(nombreServicio,' - ',DATE_FORMAT(fechaServicio,'%d-%m-%Y'),' de ',horaInicio,' a ',horaFin) as Servicios,Activo FROM servicios";
$res = mysqli_query($sql,$q);
$idserm = $_POST["serviciosdisp"];
$activo = $_POST["activo"];
?>
<!doctype html>
<html lang="en" class="h-100">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <title>Protección Civil - Servicios</title>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <!-- Bootstrap core CSS -->
<link href="css/bootstrap.css" rel="stylesheet">


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="css/sticky-footer-navbar.css" rel="stylesheet">
  </head>
  <body class="d-flex flex-column h-100">
    <header>
  <!-- Fixed navbar -->
  <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="#">Protección Civil</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="index.php">Inicio <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Información</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="login.php">Servicios</a>
          </li>
          <li class="nav-item">
              <a class="nav-link" href="login.php">Panel de administración</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Colaboraciones</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="#">Legislación</a>
                </li>
      </ul>
    </div>
  </nav>
</header>

<!-- Begin page content -->
<main role="main" class="flex-shrink-0">
  <div class="container">
  <form name="altaservicio" class="form-horizontal" action="<?php echo $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data" method="POST">
    <fieldset>
    <legend>Servicios disponibles para modificar</legend>	
    </fieldset>
        <div class="form-row">
            <div class="form-group col-md-6">
               	<div class="alert alert-info" role="alert">
		Para modificar a un servicio, toca el servicio de la lista de aqui abajo
		</div>
		 <label for="tiposervicio">Servicios disponibles</label>
                 <select multiple class="form-control" name="serviciosdisp" id="serviciosdisp" onchange="getDatosServicio(this.value);">
                <?php
                while($fila=mysqli_fetch_assoc($res)){
                  echo "<option value='".$fila['idServicio']."'>".$fila['Servicios']."</option>";
               }
                ?>
               </select>
            </div>
        </div>
        <div class="form-row">
                 <div id="datosser" name="datosser" class="form-group col-md-6">
                
                </div>
                <script>
                    function getDatosServicio(valor){
                       $.ajax({
                        url: "includes/dmserv.php",
                        type: 'GET',
                        data: {tipo_id:valor},
                           success: function(result) {
                               //console.log(result);
                             $('#datosser').html(result);
                               },
                       error: function(request, error, message) {
                        // error
                       }
                        });
                    }
                    </script>
        </div>
       <div class="form-group col-md-7">
            
        </div>
  </div>
</main>

<footer class="footer mt-auto py-3">
  <div class="container">
    <span class="text-muted">Place sticky footer content here.</span>
  </div>
</footer>
 <script src="js/bootstrap.bundle.min.js"></script></body>
</html>
<?php
 if($_SERVER['REQUEST_METHOD'] == 'POST'){
    if($activo!=1){
        $activo = 0;
    }
    $query = "UPDATE servicios SET Activo = $activo WHERE idServicio = $idserm"; 
    $res = mysqli_query($sql,$query);
    if(!$response){
        $msg = "";
        $msg.= "<div class='form-group col-md-6'>";
        $msg.="<div class='alert alert-danger' role='alert'>
     Error al introducir los datos: ";
    $msg.="</div>";
    }
}
?>