<?php
session_start();
include("includes/config.php");
if(!isset($_SESSION["admin"]) && !isset($_SESSION["usuario"])){
    if(isset($_SESSION["usuario"]) && ($_SESSION["DNI"])){
        header("Location: panelvoluntarios.php");
    } 
     header("Location: login.php");
   }
?>
<!doctype html>
<html lang="en" class="h-100">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <title>Protección Civil - Alta de voluntario</title>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="css/sticky-footer-navbar.css" rel="stylesheet">
</head>
<body class="d-flex flex-column h-100">
<header>
    <!-- Fixed navbar -->
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="#">Protección Civil</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="index.php">Inicio <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Información</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="login.php">Servicios</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="login.php">Panel de administración</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Colaboraciones</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Legislación</a>
                </li>
            </ul>
        </div>
    </nav>
</header>

<!-- Begin page content -->
<main role="main" class="flex-shrink-0">
    <div class="container">
        <form name="altaservicio" class="form-horizontal" action="<?php echo $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data" method="POST">
            <fieldset>
                <legend>Alta de nuevo servicio</legend>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="tiposervicio">Tipo del servicio</label>
                        <select class="form-control" name="tiposervicio" id="tiposervicio" onchange="getSubtipos(this.value);">
                            <?php
                            $q2 = "SELECT * FROM tipos_servicio";
                            $resu = mysqli_query($sql,$q2);
                            while($fila = mysqli_fetch_assoc($resu)){
                              echo "<option value='".$fila['idTipo']."'>".$fila['Descripcion']."</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="subtiposervicio">Subtipo del servicio</label>
                        <select class="form-control" name="subtiposervicio" id="subtiposervicio">

                        </select>
                    </div>
                    <script>
                    function getSubtipos(valor){
                       $.ajax({
                        url: "includes/dsubtipo.php",
                        type: 'GET',
                        data: {tipo_id:valor},
                           success: function(result) {
                               //console.log(result);
                             $('#subtiposervicio').html(result);
                               },
                       error: function(request, error, message) {
                        // error
                       }
                        });
                    }
                    </script>
                    <div class="form-group col-md-6">
                        <label for="nombreservicio">Nombre del Servicio</label>
                        <input type="text" class="form-control" id="nombreservicio" name="nombreservicio">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="fechaservicio">Fecha del servicio</label>
                        <input type="date" class="form-control" id="fechaservicio" name="fechaservicio" value="<?php date('d-m-Y');?>">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="nombre">Hora de inicio en Base</label>
                        <input type="text" class="form-control" id="horabase" name="horabase">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="apellidos">Hora de inicio del Servicio</label>
                        <input type="text" class="form-control" id="horainicio" name="horainicio">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="direccion">Hora de finalización del Servicio</label>
                        <input type="text" class="form-control" id="horafin" name="horafin">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="telefono">Observaciones</label>
                        <textarea rows="5" cols="50" name="observaciones"></textarea>
                    </div>
		<div class="form-group col-md-6">
			<label for="activo">Activo</label>
			<input type="checkbox" value="1" name="activo">
		</div>
                <div class="form-group col-md-6">
                    <button type="submit" class="btn btn-primary">Dar de alta</button>
                </div>
        </form>
        </fieldset>
    </div>
</main>

<footer class="footer mt-auto py-3">
    <div class="container">
        <span class="text-muted">Place sticky footer content here.</span>
    </div>
</footer>
<script src="js/bootstrap.bundle.min.js"></script>
</body>
</html>
<?php
function darDeAlta(){
    global $sql;
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        //Se recogen los datos del formulario
        $tiposer = $_POST["tiposervicio"];
        $subtiposer = $_POST["subtiposervicio"];
        $nameser = $_POST["nombreservicio"];
        $dateser = $_POST["fechaservicio"];
        $hsede = $_POST["horabase"];
        $hinicio = $_POST["horainicio"];
        $hfin = $_POST["horafin"];
	$activo = (int)($_POST["activo"]);
        $obs = mysqli_real_escape_string($sql,$_POST["observaciones"]);
        // Debug
        //echo $username;
        //echo $passwd;
        // Se realiza la consulta a la base de datos
        $q1 = "INSERT INTO servicios (nombreServicio,fechaServicio,horaSede,horaInicio,horaFin,TipoServicio,SubtipoServicio,Observaciones,Activo) VALUES ('$nameser','$dateser','$hsede','$hinicio','$hfin',$tiposer,$subtiposer,'$obs',$activo)";
        $res = mysqli_query($sql,$q1);
        if(!$res){
            echo "<p>Error en la consulta!</p>";
            echo mysqli_error($sql);

        } else{
            echo "<script>alert('Insertado con exito')</script>";
        }
    }

}
darDeAlta();
?>
